import EventServices from "../services/event";
import { validateId, validateEvent } from "../validations/event";

const {
  addEvent, addActor, addRepo, getEvent, deleteEvent, userExist, repoExist, getEvents
} = EventServices;

/**
 * @class EventController
 * @description create Event, get all Events, get a Event, delete a Event, update a Event
 * @exports EventController
 */
export default class EventController {
  /**
   * @param {object} req - The user request object
   * @param {object} res - The user response object
   * @returns {object} Success message
   */
  static async addEvent(req, res) {
    try {
      const { id } = req.decoded.user;
      const { name } = req.body;
      const { error } = validateEvent(req.body);
      if (error) return res.status(400).json({ status: 400, error: error.message });
      const user = await userExist(id);
      if (!user) return res.status(404).json({ status: 404, error: "User does not exist." });
      const repo = await repoExist(name);
      if (!repo) return res.status(404).json({ status: 404, error: "Repo does not exist." });
      const EventId = await Math.floor((Math.random() * 2000000) + 2000000);
      const ActorId = await Math.floor((Math.random() * 2000000) + 2000000);
      const RepoId = await Math.floor((Math.random() * 2000000) + 2000000);
      const newEvent = { id: EventId, actorId: ActorId };
      const event = await addEvent(newEvent);
      const newActor = {
        id: ActorId,
        eventId: event.id,
        login: user.login,
        avatar_url: user.avatar_url,
        password: user.password
      };
      const newRepo = {
        id: RepoId,
        eventId: event.id,
        name,
        url: repo.url
      };
      await addActor(newActor);
      await addRepo(newRepo);
      const createdEvent = {
        id: event.id,
        type: event.type,
        actor: { id, login: user.login, avatar_url: user.avatar_url },
        repo: { id: repo.id, name, url: repo.url },
      };
      return res.status(201).json({
        status: 201,
        message: "A Event has been added.",
        data: createdEvent,
      });
    } catch (error) {
      return res.status(500).json({ status: 500, error: error.message });
    }
  }

  /**
   * @param {object} req - The user request object
   * @param {object} res - The user response object
   * @returns {object} Success message-
   */
  static async getEvents(req, res) {
    try {
      const Event = await getEvents();
      return res.status(200).json({
        status: 200,
        message: "Successfully retrieved All Events.",
        data: Event,
      });
    } catch (error) {
      return res.status(404).json({
        status: 404,
        error: "Resource not found."
      });
    }
  }

  /**
   * @param {object} req - The user request object
   * @param {object} res - The user response object
   * @returns {object} Success message
   */
  static async getEventByActorId(req, res) {
    try {
      const { id } = req.params;
      const { error } = validateId({ id });
      if (error) return res.status(400).json({ status: 400, error: error.message });
      const Event = await getEvent(id);
      if (!Event) return res.status(404).json({ status: 404, error: "Event not found" });
      return res.status(200).json({
        status: 200,
        message: "Successfully retrieved Event.",
        data: Event,
      });
    } catch (error) {
      return res.status(404).json({
        status: 404,
        error: "Resource not found."
      });
    }
  }

  /**
     * @param {object} req - The user request object
     * @param {object} res - The user response object
     * @returns {object} Success message
     */
  static async deleteEvent(req, res) {
    try {
      await deleteEvent();
      return res.status(200).json({
        status: 200,
        message: "Successfully Deleted Events.",
      });
    } catch (error) {
      return res.status(404).json({
        status: 404,
        error: "Resource not found.",
      });
    }
  }
}
