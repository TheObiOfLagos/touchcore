import bcrypt from "bcryptjs";
import dotenv from "dotenv";
import { loginValidation, updateValidation } from "../validations/actor";
import Actor from "../services/actor";
import jwtHelper from "../utilities/jwt";

dotenv.config();

const {
  loginExist, getAllActors, updateUrl, userExist, getAllActorsByStreak
} = Actor;
const { generateToken } = jwtHelper;

/**
 * @class ActorController
 * @description create, verify and log in Actor
 * @exports ActorController
 */
export default class ActorController {
  /**
   * @param {object} req - The Actor request object
   * @param {object} res - The Actor response object
   * @returns {object} Success message
   */
  static async loginActor(req, res) {
    try {
      const { error } = loginValidation(req.body);
      if (error) return res.status(400).json({ status: 400, error: error.message });
      const { login, password } = req.body;
      const Login = login.toLowerCase();
      const user = await loginExist(Login);
      if (!user) return res.status(404).json({ status: 404, error: "User does not exist." });
      const validpass = await bcrypt.compare(password, user.password);
      if (!validpass) return res.status(400).json({ status: 400, error: "Password is not correct!." });
      const token = await generateToken({ user });
      return res.status(200).json({
        status: 200,
        message: "User Logged in Successfully.",
        data: token
      });
    } catch (error) {
      return res.status(500).json({ status: 500, error: "Server error," });
    }
  }

  /**
   * @param {object} req - The Actor request object
   * @param {object} res - The Actor response object
   * @returns {object} Success message
   */
  static async getActors(req, res) {
    try {
      const Actors = await getAllActors();
      return res.status(200).json({ status: 200, message: "Successfully retrieved all Actors.", data: Actors });
    } catch (error) {
      return res.status(500).json({ status: 500, error: "Resource not found." });
    }
  }

  /**
   * @param {object} req - The Actor request object
   * @param {object} res - The Actor response object
   * @returns {object} Success message
   */
  static async getActorsByStreak(req, res) {
    try {
      const Actors = await getAllActorsByStreak();
      return res.status(200).json({
        status: 200,
        message: "Successfully retrieved all Actors by Streaks.",
        data: Actors
      });
    } catch (error) {
      return res.status(500).json({ status: 500, error: "Resource not found." });
    }
  }

  /**
   * @param {object} req - The user request object
   * @param {object} res - The user response object
   * @returns {object} Success message
   */
  static async updateActorUrl(req, res) {
    try {
      const { error } = updateValidation(req.body);
      if (error) return res.status(400).json({ status: 400, error: error.message });
      const { id, login, active_url } = req.body;
      const user = await userExist(id);
      if (!user) return res.status(404).json({ status: 404, error: "User does not exist." });
      const updatedUser = { id, login, active_url };
      await updateUrl(id, updatedUser);
      return res.status(200).json({
        status: 200,
        message: "Successfully updated User.",
      });
    } catch (error) {
      return res.status(404).json({
        status: 404,
        error: "Resource not found.",
      });
    }
  }
}
