const { BIGINT } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  const Repo = sequelize.define("Repos",{  
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      unique: true,
    },
    eventId: {
      type: DataTypes.BIGINT,
      unique: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  });
  Repo.associate = models => {
  Repo.belongsTo(models.Events, {
      as: "repo",
      foreignKey: "eventId",
    });
  };
  return Repo;
};
