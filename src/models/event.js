const { BIGINT } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define("Events",{  
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull:false,
      unique: true,
    },
    type: {
      type: DataTypes.ENUM("PushEvent", "Merge"),
      defaultValue: "PushEvent",
      allowNull: false,
    },
    actorId: {
      type: DataTypes.BIGINT,
      unique: true,
    },
  });
  Event.associate = models => {
    Event.hasMany(models.Actors, {
      as: "actor",
      foreignKey: "eventId",
      onDelete: "cascade",
      hooks: true,
    });
    Event.hasMany(models.Repos, {
      as: "repo",
      foreignKey: "eventId",
      onDelete: "cascade",
      hooks: true,
    });
  };
  return Event;
};
