const { BIGINT } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  const Actor = sequelize.define("Actors", {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      unique: true,
    },
    eventId: {
      type: DataTypes.BIGINT,
      unique: true,
    },
    login: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    avatar_url: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  });
  Actor.associate = models => {
  Actor.belongsTo(models.Events, {
      as: "actor",
      foreignKey: "eventId",
    });
  };
  return Actor;
};

