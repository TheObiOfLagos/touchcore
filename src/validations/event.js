import Joi from "joi";

const validateId = ids => {
  const schema = Joi.object({
    id: Joi.number().integer().required().empty()
      .messages({
        "any.required": "An id is required.",
        "integer.empty": "Id cannot be an empty field.",
        "integer.base": "Please provide a valid number."
      }),
  }).messages({
    "object.unknown": "You have used an invalid key."
  }).options({ abortEarly: false });
  return schema.validate(ids);
};

const validateEvent = event => {
  const schema = Joi.object({
    name: Joi.string().required().min(5)
      .max(100)
      .empty()
      .messages({
        "any.required": "Sorry, name is required",
        "string.empty": "Sorry, name cannot be an empty field",
      }),
  }).messages({
    "object.unknown": "You have used an invalid key."
  });
  return schema.validate(event);
};
export { validateId, validateEvent };
