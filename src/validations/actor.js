import Joi from "joi";

const loginValidation = user => {
  const schema = Joi.object({
    login: Joi.string().required().min(5)
      .max(100)
      .empty()
      .messages({
        "any.required": "Sorry, login is required",
        "string.empty": "Sorry, Login cannot be an empty field",
      }),
    password: Joi.string().required().min(5).max(1024)
      .pattern(new RegExp("^[a-zA-Z0-9]{3,30}$"))
      .messages({
        "string.pattern.base": "avatar_url must contain only alphanumeric characters.",
        "string.empty": "Sorry, avatar_url cannot be an empty field",
        "string.min": "avatar_url should have a minimum length of 5"
      }),
  }).messages({
    "object.unknown": "You have used an invalid key."
  });
  return schema.validate(user);
};

const updateValidation = user => {
  const schema = Joi.object({
    id: Joi.number().integer().required().empty()
      .messages({
        "any.required": "An id is required.",
        "integer.empty": "Id cannot be an empty field.",
        "integer.base": "Please provide a valid number."
      }),
    login: Joi.string().required().min(5)
      .max(100)
      .empty()
      .messages({
        "any.required": "Sorry, login is required",
        "string.empty": "Sorry, Login cannot be an empty field",
      }),
    avatar_url: Joi.string().required().min(5).max(1024)
      .messages({
        "any.required": "Sorry, login is required",
        "string.empty": "Sorry, avatar_url cannot be an empty field",
      }),
  }).messages({
    "object.unknown": "You have used an invalid key."
  });
  return schema.validate(user);
};

export { loginValidation, updateValidation };
