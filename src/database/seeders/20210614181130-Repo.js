module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("Repos", [
      {
      id:478747,
      eventId: 1319379787,
      name:"ngriffin/rerum-aliquam-cum",
      url: "https://github.com/ngriffin/rerum-aliquam-cum",
      createdAt: new Date(),
      updatedAt: new Date(),
     },
    {
      id:451024,
      eventId: 1514531484,
      name:"daniel51/quo-tempore-dolor",
      url: "https://github.com/daniel51/quo-tempore-dolor",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id:310964,
      eventId: 1536363444,
      name:"brownphilip/rerum-quidem",
      url: "https://github.com/brownphilip/rerum-quidem",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 275832,
      eventId: 1838493121,
      name:"elizabethbailey/error-quod-a",
      url:"https://github.com/elizabethbailey/error-quod-a",
      createdAt: new Date(),
      updatedAt: new Date(),
      },
    {
      id:292520,
      eventId: 1979554031,
      name:"svazquez/dolores-quidem",
      url:"https://github.com/svazquez/dolores-quidem",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id:426482,
      eventId: 2712153979,
      name:"pestrada/voluptatem",
      url:"https://github.com/pestrada/voluptatem",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 425512,
      eventId: 3822562012,
      name: "cohenjacqueline/quam-autem-suscipit",
      url: "https://github.com/cohenjacqueline/quam-autem-suscipit",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id:352806,
      eventId: 4055191679,
      name:"johnbolton/exercitationem",
      url:"https://github.com/johnbolton/exercitationem",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id:301227,
      eventId: 4501280090,
      name:"oscarschmidt/doloremque-expedita",
      url: "https://github.com/oscarschmidt/doloremque-expedita",
      createdAt: new Date(),
      updatedAt: new Date(),
      
    },
    {
      id:269910,
      eventId: 4633249595,
      name:"iholloway/aperiam-consectetur",
      url:"https://github.com/iholloway/aperiam-consectetur",
      createdAt: new Date(),
      updatedAt: new Date(),
    }
    ,], 
    {});
  },

  down: async (queryInterface, Sequelize) => {

  },
};
