import bcrypt from "bcryptjs";

const password = "12345";
const hash = bcrypt.hashSync(password, 10);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("Actors", [{
      id: 4276597,
      eventId: 1319379787,
      login: "iholloway",
      password: hash,
      avatar_url:"https://avatars.com/4276597",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id:2917996,
      eventId: 1514531484,
      login:"oscarschmidt",
      avatar_url:"https://avatars.com/2917996",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id:2790311,
      eventId: 1536363444,
      login:"daniel33",
      avatar_url:"https://avatars.com/2790311",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 2222918,
      eventId: 1838493121,
      login:"xnguyen",
      avatar_url:"https://avatars.com/2222918",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
      },
    {
      id:2907782,
      eventId: 1979554031,
      login:"eric66",
      avatar_url: "https://avatars.com/2907782",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
  },
  {
      id:3648056,
      eventId: 2712153979,
      login:"ysims",
      avatar_url: "https://avatars.com/modified2",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
  },
  {
      id:4864659,
      eventId: 3822562012,
      login:"katrinaallen",
      avatar_url: "https://avatars.com/4864659",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
  },
  {
      id:4949434,
      eventId: 4055191679,
      login:"millerlarry",
      avatar_url: "https://avatars.com/4949434",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
  },
  {
      id:3698252,
      eventId: 4501280090,
      login:"daniel51",
      avatar_url: "https://avatars.com/3698252",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
  },
  {
      id:3466404,
      eventId: 4633249595,
      login:"khunt",
      avatar_url: "https://avatars.com/3466404",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date(),
  }

    ],
    {});
  },

  down: async (queryInterface, Sequelize) => {

  },
};
