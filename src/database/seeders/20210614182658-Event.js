module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("Events", [
      {
      id: 1319379787,
      actorId: 4276597,
      type: "PushEvent",
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      id: 1514531484,
      actorId: 2917996,
      type: "PushEvent",
      createdAt: new Date(),
      updatedAt: new Date(),
      },
      {
      id: 1536363444,
      actorId: 2790311,
      type :"PushEvent",
      createdAt: new Date(),
      updatedAt: new Date(),
      },
      {
      id: 1838493121,
      actorId: 2222918,
      type:"PushEvent",
      createdAt: new Date(),
      updatedAt: new Date(),
      },
      {
    id: 1979554031,
    actorId: 2907782,
    type: "PushEvent",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 2712153979,
    actorId: 3648056,
    type: "PushEvent",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 3822562012,
    actorId: 4864659,
    type: "PushEvent",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 4055191679,
    actorId: 4949434,
    type:"PushEvent",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 4501280090,
    actorId: 3698252,
    type: "PushEvent",
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 4633249595,
    actorId: 3466404,
    type: "PushEvent",
    createdAt: new Date(),
    updatedAt: new Date(),
  }
    ,], 
    {});
  },

  down: async (queryInterface, Sequelize) => {

  },
};
