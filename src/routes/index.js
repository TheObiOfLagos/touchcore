import { Router } from "express";
import actorRoutes from "./actorRoutes";
import eventRoutes from "./eventRoutes";

const router = new Router();

router.use("/", actorRoutes);
router.use("/", eventRoutes);

export default router;
