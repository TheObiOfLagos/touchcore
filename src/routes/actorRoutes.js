import { Router } from "express";
import ActorController from "../controllers/actor";
import Authentication from "../middlewares/authenticate";

const router = Router();
const { verifyToken } = Authentication;
const {
  loginActor, getActors, getActorsByStreak, updateActorUrl
} = ActorController;

router.post("/users/signin", loginActor);

router.get("/actors", verifyToken, getActors);
router.get("/actors/streak", verifyToken, getActorsByStreak);

router.put("/actors", verifyToken, updateActorUrl);

export default router;
