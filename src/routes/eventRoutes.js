import { Router } from "express";
import EventController from "../controllers/event";
import Authentication from "../middlewares/authenticate";

const router = Router();
const { verifyToken } = Authentication;
const {
  addEvent, getEvents, getEventByActorId, deleteEvent,
} = EventController;

router.get("/events/actors/:id", verifyToken, getEventByActorId);
router.get("/events", verifyToken, getEvents);

router.post("/events", verifyToken, addEvent);

router.delete("/erase", verifyToken, deleteEvent);

export default router;
