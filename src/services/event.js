import database from "../models";

/**
 * @class Event
 * @description allows user create and check Event details
 * @exports Event
 */
export default class EventServices {
  /**
   *  @param {object} newEvent - The Actor details
   * @returns {object} An instance of the Events model class
   */
  static async addEvent(newEvent) {
    try {
      return await database.Events.create(newEvent);
    } catch (err) {
      throw err;
    }
  }
  /**
   * @param {object} newActor - The Actor details
   * @returns {object} An instance of the Events model class
   */

  static async addActor(newActor) {
    try {
      return await database.Actors.create(newActor);
    } catch (err) {
      throw err;
    }
  }
  /**
   * @param {object} newRepo - The Repo details
   * @returns {object} An instance of the Events model class
   */

  static async addRepo(newRepo) {
    try {
      return await database.Repos.create(newRepo);
    } catch (err) {
      throw err;
    }
  }

  /**
   * @param {string} id - The Event id
   * @returns {object} An instance of the Events model class
   */
  static async getEvent(id) {
    try {
      return await database.Events.findOne({
        where: {
          actorId: id
        },
        attributes: [
          "id", "type", "createdAt"
        ],
        include: [
          {
            model: database.Actors,
            as: "actor",
            attributes: [
              "id", "login", "avatar_url"
            ],
          },
          {
            model: database.Repos,
            as: "repo",
            attributes: [
              "id", "name", "url"
            ],
          },
        ]
      });
    } catch (err) {
      throw err;
    }
  }

  /**
   * @returns {object} An instance of the Events model class
   */
  static async getEvents() {
    try {
      return await database.Events.findAll({
        attributes: [
          "id", "type", "createdAt"
        ],
        include: [
          {
            model: database.Actors,
            as: "actor",
            attributes: [
              "id", "login", "avatar_url"
            ],
          },
          {
            model: database.Repos,
            as: "repo",
            attributes: [
              "id", "name", "url"
            ],
          },
        ]
      });
    } catch (err) {
      throw err;
    }
  }

  /**
   * @param {string} id - The user id
   * @returns {object} An instance of the Events model class
   */
  static async userExist(id) {
    try {
      return await database.Actors.findOne({
        where: {
          id
        }
      });
    } catch (err) {
      throw err;
    }
  }

  /**
   * @param {string} name - The Repo id
   * @returns {object} An instance of the Events model class
   */
  static async repoExist(name) {
    try {
      return await database.Repos.findOne({
        where: {
          name
        }
      });
    } catch (err) {
      throw err;
    }
  }

  /**
   * @param {string} id - The Event name
   * @returns {object} An instance of the Events model class
   */
  static async deleteEvent(id) {
    try {
      return await database.Events.destroy({
        where: {},
        truncate: true
      });
    } catch (err) {
      throw err;
    }
  }
}
