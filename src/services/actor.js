import database from "../models";

/**
 * @class User
 * @description User services
 * @exports User
 */
export default class Actor {
  /**
   * @returns {object} An instance of the Actor class
   */
  static async getAllActors() {
    try {
      //
      return await database.Actors.findAll({
        attributes: [
          "id", "login", "avatar_url"
        ],
      });
    } catch (err) {
      throw err;
    }
  }

  /**
   * @param {string} login  - The user email
   * @returns {object} - An instance of the Users model class
   */
  static async loginExist(login) {
    try {
      return await database.Actors.findOne({
        where: {
          login
        }
      });
    } catch (error) {
      throw error;
    }
  }

  /**
   * @param {string} id - The old user id
   * @param {string} Url - The new actor  details
   * @returns {object} An instance of the User model class
   */
  static async updateUrl(id, Url) {
    try {
      return await database.Actors.update(Url, {
        where: { id },
        returning: true,
        plain: true
      });
    } catch (err) {
      throw err;
    }
  }/**
   * @param {string} id - The user id
   * @returns {object} An instance of the User model class
   */

  static async userExist(id) {
    try {
      return await database.Actors.findOne({
        where: {
          id
        }
      });
    } catch (err) {
      throw err;
    }
  }/**
   * @param {string} id - The old state name
   * @returns {object} An instance of the User model class
   */

  static async getAllActorsByStreak() {
    try {
      return await database.Actors.findAll({
        attributes: [
          "id", "login", "avatar_url"
        ],
        group: ["Actors.id"],
        order: [
          ["count", "DESC"]
        ],
      });
    } catch (err) {
      throw err;
    }
  }
}
