import {
  sequelize,
  dataTypes,
  checkModelName,
  checkPropertyExists,
} from "sequelize-test-helpers";
import chai, { expect } from "chai";

import sinonChai from "sinon-chai";
import EventModel from "../../models/event";

chai.use(sinonChai);

describe("src/models/event", () => {
  const Event = EventModel(sequelize, dataTypes);
  const event = new Event();

  checkModelName(Event)("Events");
  context("properties", () => {
    ["id", "type"].forEach(
      checkPropertyExists(event),
    );
  });
});
