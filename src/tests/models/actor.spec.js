import chai, { expect } from "chai";

import sinonChai from "sinon-chai";
import {
  sequelize,
  dataTypes,
  checkModelName,
  checkPropertyExists,
} from "sequelize-test-helpers";

import ActorModel from "../../models/actor";

chai.use(sinonChai);
describe("src/models/actor", () => {
  const Actor = ActorModel(sequelize, dataTypes);
  const actor = new Actor();
  checkModelName(Actor)("Actors");
  context("properties", () => {
    ["id", "login", "password", "eventId", "avatar_url"].forEach(
      checkPropertyExists(actor),
    );
  });
});
