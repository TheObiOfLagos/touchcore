import {
  sequelize,
  dataTypes,
  checkModelName,
  checkPropertyExists,
} from "sequelize-test-helpers";
import chai, { expect } from "chai";

import sinonChai from "sinon-chai";
import RepoModel from "../../models/repo";

chai.use(sinonChai);

describe("src/models/repo", () => {
  const Repo = RepoModel(sequelize, dataTypes);
  const repo = new Repo();

  checkModelName(Repo)("Repos");
  context("properties", () => {
    ["id", "eventId", "name", "url"].forEach(
      checkPropertyExists(repo),
    );
  });
});
