import chai from "chai";
import chaiHttp from "chai-http";
import { user4 } from "./user-sign-in-test-data";
import { event, event2, event3 } from "./event-data";
import server from "../../app";

chai.should();

const { expect } = chai;
chai.use(chaiHttp);

describe("Add events", () => {
  let userToken;
  before(done => {
    chai
      .request(server)
      .post("/users/signin")
      .set("Accept", "application/json")
      .send(user4)
      .end((err, res) => {
        if (err) throw err;
        userToken = res.body.data;
        done();
      });
  });
  it("should allow user with token add a event", done => {
    chai
      .request(server)
      .post("/events")
      .set("Authorization", `Bearer ${userToken}`)
      .set("Accept", "application/json")
      .send(event)
      .end((err, res) => {
        expect(res).to.have.status(201);
        done();
      });
  });
  it("should not allow user add a event with incomplete details", done => {
    chai
      .request(server)
      .post("/events")
      .set("Authorization", `Bearer ${userToken}`)
      .set("Accept", "application/json")
      .send(event2)
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });
  it("should not allow user without token add a event ", done => {
    chai
      .request(server)
      .post("/events")
      .send(event2)
      .end((err, res) => {
        expect(res).to.have.status(401);
        done();
      });
  });
});

describe("GET events api route", () => {
  let userToken;
  before(done => {
    chai
      .request(server)
      .post("/users/signin")
      .set("Accept", "application/json")
      .send(user4)
      .end((err, res) => {
        if (err) throw err;
        userToken = res.body.data;
        done();
      });
  });
  it("returns all events ", done => {
    chai
      .request(server)
      .get("/events")
      .set("Authorization", `Bearer ${userToken}`)
      .end((err, res) => {
        const { status, body } = res;
        const { data } = body;
        expect(status).to.equal(200);
        expect(body.status).to.equal(200);
        expect(body.message).to.equal("Successfully retrieved All Events.");

        data.forEach(events => {
          expect(events).to.have.property("id");
          expect(events).to.have.property("type");
        });

        expect(data).to.be.an("array");
        done();
      });
  });

  it("returns event with specific actor id", done => {
    chai
      .request(server)
      .get("/events/actors/4276597")
      .set("Authorization", `Bearer ${userToken}`)
      .end((err, res) => {
        const { status, body } = res;
        const { data } = body;
        expect(status).to.equal(200);
        expect(body.status).to.equal(200);
        expect(body.message).to.equal("Successfully retrieved Event.");
        expect(body.message).to.equal("Successfully retrieved Event.");
        expect(data).to.have.property("id");
        expect(data).to.have.property("createdAt");

        expect(data).to.be.an("object");
        done();
      });
  });
});

describe("Delete events", () => {
  let userToken;
  before(done => {
    chai
      .request(server)
      .post("/users/signin")
      .set("Accept", "application/json")
      .send(user4)
      .end((err, res) => {
        if (err) throw err;
        userToken = res.body.data;
        done();
      });
  });
  it("should allow User Delete all events", done => {
    chai
      .request(server)
      .delete("/erase")
      .set("Authorization", `Bearer ${userToken}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.message).to.equal("Successfully Deleted Events.");
        done();
      });
  });
});
