const event = {
  name: "ngriffin/rerum-aliquam-cum",
};
const event2 = {
  tame: "PusEvent",
};

const event3 = {
  id: 4501280090,
  type: "PushEvent",
};

const event4 = {
  id: 2575717,
  type: "PushEvent",
  actor: {
    id: 2222918,
    login: "xnguyen",
    avatar_url: "https://avatars.com/2222918"
  },
  repo: {
    id: 352806,
    name: "johnbolton/exercitationem",
    url: "https://github.com/johnbolton/exercitationem"
  }
};

const event5 = {
  id: 2712153979,
  type: "PushEvent",
  createdAt: new Date(),
  updatedAt: new Date(),
};

export {
  event, event2, event3, event4, event5
};
