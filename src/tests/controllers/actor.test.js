import chai from "chai";
import chaiHttp from "chai-http";
import { user4 } from "./user-sign-in-test-data";
import { actor, actor2, actor3 } from "./actor-data";
import server from "../../app";

chai.should();

const { expect } = chai;
chai.use(chaiHttp);

describe("Update Actors Url", () => {
  let userToken;
  before(done => {
    chai
      .request(server)
      .post("/users/signin")
      .set("Accept", "application/json")
      .send(user4)
      .end((err, res) => {
        if (err) throw err;
        userToken = res.body.data;
        done();
      });
  });
  it("should allow User Update Actor Url", done => {
    chai
      .request(server)
      .put("/actors")
      .set("Authorization", `Bearer ${userToken}`)
      .send(actor)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.message).to.equal("Successfully updated User.");
        done();
      });
  });
  it("should not allow user update an actor with invalid details", done => {
    chai
      .request(server)
      .put("/actors")
      .set("Authorization", `Bearer ${userToken}`)
      .send(actor2)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body.error).to.equal("An id is required.");
        done();
      });
  });
  it("returns 404 when updating actor which is not in db", done => {
    chai
      .request(server)
      .put("/actors")
      .set("Authorization", `Bearer ${userToken}`)
      .send(actor3)
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.body.error).to.equal("User does not exist.");
        done();
      });
  });
});

describe("GET actor api route", () => {
  let userToken;
  before(done => {
    chai
      .request(server)
      .post("/users/signin")
      .set("Accept", "application/json")
      .send(user4)
      .end((err, res) => {
        if (err) throw err;
        userToken = res.body.data;
        done();
      });
  });
  it("returns all actors", done => {
    chai
      .request(server)
      .get("/actors")
      .set("Authorization", `Bearer ${userToken}`)
      .end((err, res) => {
        const { status, body } = res;
        const { data } = body;
        expect(status).to.equal(200);
        expect(body.status).to.equal(200);
        expect(body.message).to.equal("Successfully retrieved all Actors.");

        data.forEach(actors => {
          expect(actors).to.have.property("id");
          expect(actors).to.have.property("login");
          expect(actors).to.have.property("avatar_url");
        });

        expect(data).to.have.length(10);

        expect(data).to.be.an("array");
        done();
      });
  });

  it("returns actor based on maximum streak", done => {
    chai
      .request(server)
      .get("/actors/streak")
      .set("Authorization", `Bearer ${userToken}`)
      .end((err, res) => {
        const { status, body } = res;
        const { data } = body;
        expect(status).to.equal(200);
        expect(body.message).to.equal("Successfully retrieved all Actors by Streaks.");
        data.forEach(actors => {
          expect(actors).to.have.property("id");
          expect(actors).to.have.property("login");
          expect(actors).to.have.property("avatar_url");
        });

        expect(data).to.be.an("array");
        done();
      });
  });
});
